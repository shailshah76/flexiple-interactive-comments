Built an Interactive comments section using React Js and deployed on Netlify

### Requirements

1. The comments section should be responsive and have optimal layout on all screen sizes.
2. A user should be able to read, post, edit & delete comments.
3. A user can reply to a comment, and a reply should be nested inside its parent comment.
4. A user should be able to upvote a comment.

@Author - Shail Shah